const App = {
    data() {
        return { 
            counter: 0,
            title: 'Список заметок',
            placeholderString: 'Введите название заметки',
            inputValue: '',
            notes: ['Zametka1', 'Zametka2', 'Zametka3']
        }
    },
    methods: {
        addNewNote(){
            if(this.inputValue !== ''){
                this.notes.push(this.inputValue)
                this.inputValue=''
            }
        },
        inputKeyPress(event){
            if(event.key=='Enter'){
                this.addNewNote()
            }
        },
        toUpperCase(item){
            return item.toUpperCase()
        },
        removeNote(i){
            this.notes.splice(i,1)
        }
    },
    computed: {
        doubleCountComputed(){
            console.log('doubleCountComputed')
            return this.notes.length * 2
        },
    },
    watch: {
        inputValue(value){
            if(value.length > 10){
                this.inputValue = ''
            }
        }
    }
}
Vue.createApp(App).mount('#app')